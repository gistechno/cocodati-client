import { Component, OnInit, Inject } from '@angular/core';
import { CoorsystemsService } from '../_services/coorsystems.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


import Map from 'ol/map';
import View from 'ol/view';
import WMTSCapabilities from 'ol/format/wmtscapabilities';
import TileLayer from 'ol/layer/tile';
import OSM from 'ol/source/osm';
import WMTS from 'ol/source/wmts'
import OlProj from 'ol/proj';
import Coordinate from 'ol/coordinate';
import Overlay from 'ol/overlay';
import TileWMS from 'ol/source/tilewms';


@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css']
})
export class ConverterComponent implements OnInit {

  
  constructor(private route: ActivatedRoute, private csService: CoorsystemsService, private modalService: NgbModal) { }

  configs:any = [];

  language_configs:any = [];
  languages = [
    {id: 0, name:'icelandic'},
    {id: 1, name: 'english'}
  ];
  language_selected = this.languages[0];
  l:any = []; //short name for language, holds text in correct language

  flag_urls = [
    "assets/img/is_thumb33x24.png",
    "assets/img/british-flag_thumb.png"
  ]
  flag_selected = this.flag_urls[1];

  system_selected = 'ISN2016';
  system_to_selected = 'ISN2016';
  sub_systems_from:any = [];
  sub_system_from_selected = 'LatLong';
  sub_systems_to:any = [];
  sub_system_to_selected = 'LCC';
  systems:any = [];
  systems_to:any = [];
  heights = ['h', 'MSL']
  height_from_selected =  'h';
  height_to_selected = 'h';

  y_cord:any = '0';
  x_cord:any = '0';
  z_cord:any = '0';

  utm_zones:any = ['26','27','28'];
  zone:any = '26';
  gk_zones:any = ['12', '15','18','21','24'];
  gk_zone:any = '18';

  latlong_formats = ['DMS', 'DM', 'Decimal'];
  latlong_format_selected = 'DMS';

  y_cord_converted: any = '0';
  x_cord_converted: any = '0';
  z_cord_converted: any = '0';

  y_from_cord_name:any = 'y';
  x_from_cord_name:any = 'x';
  z_from_cord_name:any = 'z';

  y_to_cord_name:any = 'y';
  x_to_cord_name:any = 'x';
  z_to_cord_name:any = 'z';

  pan_cord_x: any = -20.0;
  pan_cord_y: any = 65.0;

  needs_zone = false;
  needs_height = false;
  
  is_file_conversion: boolean = false;

  email:string = '';
  username:string = '';
  fileToUpload: File = null;
  file_header:string = '0';

  closeResult: string;

  inputInfo: string = '';

  filesent_status = 'No file sent';

  pop_up_message = {
    header: " ",
    message: " "
  }

  printss(){
    console.log("clicked");
  }

  ngOnInit() {
    this.updateLanguage();
    this.csService.getConfig().subscribe(configs => {
      this.configs = configs;
      /*for(let x in this.configs['systemsinfo']){
        this.sub_systems_from.push(x);
        this.sub_systems_to.push(x);
      }
      this.systems = ['ISN93', 'ISN2004', 'ISN2016'];
      this.systems_to= ['ISN93', 'ISN2004', 'ISN2016'];
      this.system_to_selected = 'ISN2016';*/
      this.initSystems();
      this.checkHeightZone();
    });

    this.updatePopUpMessage();
    

    var parser = new WMTSCapabilities();

    //fetch('http://jardbakki.lmi.is:8088/cocodati-server/api/config/1/getwmts/').then((response) => {
    fetch('/cocodati-server/api/config/1/getwmts/').then((response) => {
      return response.text();
    }).then((text) => {
      var result = parser.read(text);
      
      var iceland = [-19,65]
      var icelandWebMercator = OlProj.fromLonLat(iceland);
      
      //causes error
      var view = new View({
        center: icelandWebMercator,
        zoom: 5
      });

      //Elements that make up the popup.
      var container = document.getElementById('popup');
      var content = document.getElementById('popup-content');
      var closer = document.getElementById('popup-closer');

      //Create an overlay to anchor the popup to the map.      
      var overlay = new Overlay({
        element: container,
        autoPan: true,
        autoPanAnimation: {
          duration: 250
        }
      });

      /**
      * Add a click handler to hide the popup.
      * @return {boolean} Don't follow the href.
      */
      closer.onclick = () => {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
      };
      var LMI_Kort = WMTS.optionsFromCapabilities(result, {
        layer: 'LMI_Kort',
        matrixSet: 'EPSG3857'
      });

      var Ornefni = WMTS.optionsFromCapabilities(result, {
        layer: 'Ornefni',
        matrixSet: 'EPSG3857'
      });
    
      var layers = [
        new TileLayer({
          opacity: 1,
          source: new WMTS(/** @type {!olx.source.WMTSOptions} */ (LMI_Kort))
        }),
        new TileLayer({
          opacity: 1,
          source: new WMTS(/** @type {!olx.source.WMTSOptions} */ (Ornefni))
        }),
        new TileLayer({
          source: new OSM(),
          visibility:false
        })
      ]
  
      layers[2].setVisible(false);

      var map = new Map({
        target: 'map',
        layers: layers,
        overlays: [overlay],
        // Improve user experience by loading tiles while animating. Will make
        // animations stutter on mobile or slow devices.
        loadTilesWhileAnimating: true,
        view: view,
        controls:[]
      });

      map.on('moveend',function(e){
        var zoomLevel = map.getView().getZoom();
        if(zoomLevel > 13){
          layers[0].setVisible(false);
          layers[2].setVisible(true);
        }
        else{
          layers[0].setVisible(true);
          layers[2].setVisible(false);
        }
        
        console.log();
      });
      
      map.on('click', (evt) => {
        var coordinate = evt.coordinate
        var hdms = OlProj.transform(
          coordinate, 'EPSG:3857', 'EPSG:4326');
        
        var tempHdms = hdms;
        
        var hdms = Coordinate.toStringHDMS(OlProj.toLonLat(coordinate));

        if(this.x_cord == '0' && this.y_cord == '0' && this.sub_system_from_selected == "LatLong"){
          var tempHdms = hdms.split("N")
          this.x_cord = tempHdms[1].substring(1); 
          this.y_cord = tempHdms[0] + "N"
        }
        content.innerHTML = '<div class="code-text">' + hdms + '</div>';
        overlay.setPosition(coordinate);
        
      });   

      function onClick(id, callback) {
        document.getElementById(id).addEventListener('click', callback);
      }
      var pan_to = OlProj.fromLonLat([-0.12755, 51.507222]);
      
      onClick('pan-to', () => {
        if(this.is_file_conversion){
          //this.convertFile();
        }
        else{
          let req = [{
            system: this.system_selected,
            system_to: this.system_to_selected,
            from: this.sub_system_from_selected,
            to: this.sub_system_to_selected,
            y: this.y_cord,
            x: this.x_cord,
            z: this.z_cord,
            zone: this.zone,
            latlong_format: this.latlong_format_selected,
            height_from:this.height_from_selected,
            height_to: this.height_to_selected,
          }];
          
          let temp;
      
          this.csService.convert(req).subscribe(res => {
            console.log(res);
            this.y_cord_converted = res['y'];
            this.x_cord_converted = res['x'];
            this.z_cord_converted = res['z'];
            this.pan_cord_x = res['latlongX'];
            this.pan_cord_y = res['latlongY'];
            
            pan_to = OlProj.fromLonLat([this.pan_cord_x, this.pan_cord_y])
            view.animate({
              center: pan_to,
              duration: 2000
            });
            var coordinate = pan_to;
            var hdms = Coordinate.toStringHDMS(OlProj.toLonLat(coordinate));
            content.innerHTML = '<p>' + hdms + '</p>';
            //content.innerHTML = hdms;
            overlay.setPosition(coordinate);
          });
        }
      });
    });

    
  }

  handleFileInput(files: FileList) {
    this.systems_to
    this.fileToUpload = files.item(0);
  }


  convertFile(){
    let req = {
      system: this.system_selected,
      system_to: this.system_to_selected,
      from: this.sub_system_from_selected,
      to: this.sub_system_to_selected,
      lines: this.file_header,
      email:this.email,
      username: this.username,
      utm_zone:this.zone,
      gk_zone:this.gk_zone,
      language: this.language_selected.name,
      height_from:this.height_from_selected,
      height_to: this.height_to_selected
    }
    this.csService.convertFile(req,this.fileToUpload).subscribe(configs => {
      this.filesent_status = configs['status'];
      this.updatePopUpMessage();
    });
  
  }

  clickHeightRadio(){
    this.initSystems();
    this.updateCoordNames();
  }

  initSystems(){
    this.systems = [];
    for(let x in this.configs['systems']){
      this.systems.push(x);
    }
    this.sub_systems_from = [];
    for(let x in this.configs['systems'][this.system_selected][this.height_from_selected]){
      this.sub_systems_from.push(x);
    }
    if(!this.sub_systems_from.some(e => e === this.sub_system_from_selected)){
      this.sub_system_from_selected = this.sub_systems_from[0];
    }
    this.systems_to = [];
    for(let x in this.configs['systems'][this.system_selected][this.height_from_selected][this.sub_system_from_selected]){
      this.systems_to.push(x);
    }
    if(!this.systems_to.some(e => e === this.system_to_selected)){
      this.system_to_selected = this.systems_to[0];
    }
    this.sub_systems_to = [];
    for(let x in this.configs['systems'][this.system_selected][this.height_from_selected][this.sub_system_from_selected][this.system_to_selected][this.height_to_selected]){
      this.sub_systems_to.push(x);
    }
    if(!this.sub_systems_to.some(e => e === this.sub_system_to_selected)){
      this.sub_system_to_selected = this.sub_systems_to[0];
    }
    this.updateInformation();
    this.checkHeightZone();
    this.updateCoordNames();
  }

  convert(){
    let req = [{
      system: this.system_selected,
      system_to: this.system_to_selected,
      from: this.sub_system_from_selected,
      to: this.sub_system_to_selected,
      y: this.y_cord,
      x: this.x_cord,
      z: this.z_cord,
      zone: this.zone,
      latlong_format: this.latlong_format_selected,
      height_from:this.height_from_selected,
      height_to: this.height_to_selected
    }];

    let temp;

    this.csService.convert(req).subscribe(res => {
      console.log(res);
      this.y_cord_converted = res['y'];
      this.x_cord_converted = res['x'];
      this.z_cord_converted = res['z'];
      this.pan_cord_x = res['latlongX'];
      this.pan_cord_y = res['latlongY'];
    })
  }

  checkHeightZone(){
    try {
      this.needs_height = this.configs['systems'][this.system_selected][this.height_from_selected][this.sub_system_from_selected][this.system_to_selected][this.height_to_selected][this.sub_system_to_selected]['height'];
      this.needs_zone = this.configs['systems'][this.system_selected][this.height_from_selected][this.sub_system_from_selected][this.system_to_selected][this.height_to_selected][this.sub_system_to_selected]['zone']
    }
    catch(err) {
    }
  }

  updatePopUpMessage(){
    if(this.filesent_status == 'Command not available'){
      this.pop_up_message.header = this.l['Error']
      this.pop_up_message.message = this.l['filesent_error_message']
    }
    else if(this.filesent_status == 'Bad File'){
      this.pop_up_message.header = this.l['Error']
      this.pop_up_message.message = this.l['filesent_file_error']
    }
    else{
      this.pop_up_message.header = this.l['filesent']
      this.pop_up_message.message = this.l['filesent_message']
    }
    
  }

  updateInformation(){
    this.inputInfo = this.l["ex"][this.sub_system_from_selected];
  }

  updateCoordNames(){
    this.y_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['y'];
    this.x_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['x'];
    this.z_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['z'];
    this.y_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['y'];
    this.x_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['x'];
    this.z_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['z'];
  }

  updateLanguage(){
    this.csService.getLanguageConfig(this.language_selected.id).subscribe(languages => {
      this.l = languages;
      this.inputInfo = this.l["ex"][this.sub_system_from_selected];
      this.updateCoordNames();
    });
  }

  changeLanguage(){
    if(this.language_selected.id == 1){
      this.language_selected = this.languages[0];
      this.flag_selected = this.flag_urls[1];
    }
    else{
      this.language_selected = this.languages[1];
      this.flag_selected = this.flag_urls[0];
    }
    this.updateLanguage();
    this.updateCoordNames();
  }

  open(content) {
    if(this.is_file_conversion){
      this.convertFile();
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        if(this.filesent_status == 'Success'){
          window.location.reload();
        }
        
      });
    }
    
    
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}

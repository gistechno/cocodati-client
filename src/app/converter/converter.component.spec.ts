import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterComponent } from './converter.component';
import { Routes, RouterModule } from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import { Observable } from 'rxjs';
import { of } from 'rxjs';

import { CoorsystemsService } from '../_services/coorsystems.service';

describe('ConverterComponent', () => {
  let component: ConverterComponent;
  let fixture: ComponentFixture<ConverterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[CoorsystemsService],
      imports:[FormsModule, RouterTestingModule, HttpClientModule],
      declarations: [ ConverterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should ', async(() =>{
    component.system_selected = 'ISN2004';
    component.system_to_selected = 'ISN2004';
    component.sub_system_from_selected = 'latlong';
    component.sub_system_to_selected = 'lcc';
    component.y_cord = '66.527414836';
    component.x_cord = '-17.981614272';
    component.z_cord = '0';
    component.zone = '0';
    
    expect(component.y_cord_converted).toBe('0');
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});


import { TestBed, inject } from '@angular/core/testing';

import { CoorsystemsService } from './coorsystems.service';
import {RouterTestingModule} from '@angular/router/testing';
import { Routes, RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';


describe('CoorsystemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoorsystemsService],
      imports:[
        HttpClientModule,
      ]
      
    });
  });

 

  it('should be created', inject([CoorsystemsService], (service: CoorsystemsService) => {
    expect(service).toBeTruthy();
  }));

});

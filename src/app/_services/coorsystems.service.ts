import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions} from '@angular/http';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class CoorsystemsService {

  constructor(private http:HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders()
  };
  
  getConfig():Observable<any>{
    
    //return this.http.get('http://jardbakki.lmi.is:8088/cocodati-server/api/config/1/getconfig/', this.httpOptions).pipe(map((res:Response) => res));
    return this.http.get('/cocodati-server/api/config/1/getconfig/', this.httpOptions).pipe(map((res:Response) => res));
  }

  getTestCases():Observable<any>{
    //return this.http.get('http://jardbakki.lmi.is:8088/cocodati-server/api/config/1/gettestcases/', this.httpOptions).pipe(map((res:Response) => res));
    return this.http.get('/cocodati-server/api/config/1/gettestcases/', this.httpOptions).pipe(map((res:Response) => res));
  }

  getLanguageConfig(id:any):Observable<any>{
    //return this.http.get('http://jardbakki.lmi.is:8088/cocodati-server/api/config/' + id + '/getlanguages/', this.httpOptions).pipe(map((res:Response) => res));
    return this.http.get('/cocodati-server/api/config/' + id + '/getlanguages/', this.httpOptions).pipe(map((res:Response) => res));
  }

  convertFile(param:any, file:File):Observable<any>{
    let body:string = JSON.stringify(param);
    let formData: FormData = new FormData();
    formData.append('uploadFile', file, file.name);
    formData.append('body', body);
  
    let params = new HttpParams();
    const options = {
      params: params,
      reportProgress: true,
    };
    //return this.http.post('http://jardbakki.lmi.is:8088/cocodati-server/api/convertfile/1/postfile/', formData, options).pipe(map((res:Response) => res));
    return this.http.post('/cocodati-server/api/convertfile/1/postfile/', formData, options).pipe(map((res:Response) => res));
  }

  convert(param:any):Observable<any>{
    let body:string = JSON.stringify(param);
    let options = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    //return this.http.post('http://jardbakki.lmi.is:8088/cocodati-server/api/convert/1/convertCoords/', body,options).pipe(map((res:Response) => res));
    return this.http.post('/cocodati-server/api/convert/1/convertCoords/', body,options).pipe(map((res:Response) => res));
  }
}

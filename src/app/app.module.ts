import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ConverterComponent } from './converter/converter.component';
import { routing }        from './app.routing';
import {CoorsystemsService} from './_services/coorsystems.service';
import { TestconversionsComponent } from './testconversions/testconversions.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ConverterComponent,
    TestconversionsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    routing,
    FormsModule,
    NgbModule
  ],
  providers: [HttpClientModule,CoorsystemsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

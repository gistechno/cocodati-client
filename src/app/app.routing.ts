import { Routes, RouterModule } from '@angular/router';

import { ConverterComponent } from './converter/converter.component';
import { TestconversionsComponent } from './testconversions/testconversions.component';


const appRoutes: Routes = [
    
    { path: '', component: ConverterComponent },
    //{ path: 'testing', component:TestconversionsComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestconversionsComponent } from './testconversions.component';

describe('TestconversionsComponent', () => {
  let component: TestconversionsComponent;
  let fixture: ComponentFixture<TestconversionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestconversionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestconversionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
